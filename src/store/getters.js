import state from './state'

export default {
  authenticatedUser: state1 => {
    // Return True if there is an authenticated user in the app. False otherwise
    return state.username !== null
  }
}
