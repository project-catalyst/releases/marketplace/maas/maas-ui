import state from './state'

export default {
  'SET_USER' (dd, payload) {
    state.user.access_token = payload.user
  },
  'REFRESH' (dd, payload) {
    state.user.refresh_token = payload.user
  },
  'CLEAR_USER' (state) {
    state.user = {}
  },
  'SET_USERNAME' (dd, payload) {
    state.username = 'NoUserName'
  },
  'CLEAR_USERNAME' (dd, payload) {
    state.username = null
  }
}
