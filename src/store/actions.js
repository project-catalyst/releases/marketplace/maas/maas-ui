import MaaSaxios from '../axios/index'
// import axios from 'axios'
import state from './state'
import router from '../router'

export default {
  setLogoutTimer ({commit, dispatch}, expirationTime) {
    setTimeout(() => {
      dispatch('logout')
      
    }, expirationTime * 1000)
  },
  login ({commit, dispatch}, credentials) {
    // Authenticate a user
    return MaaSaxios.post('/token/', credentials)
      .then(response => {
        const now = new Date()
        const expirationTime = new Date(now.getTime() + response.data.auth.expires_in * 1000)
        localStorage.setItem('access_token', response.data.auth.access_token)
        localStorage.setItem('refresh_token', response.data.auth.refresh_token)
        localStorage.setItem('expirationTime', expirationTime)
        commit('SET_USER', {user: response.data.auth.access_token})
        commit('REFRESH', {user: response.data.auth.refresh_token})
        commit('SET_USERNAME')
        dispatch('setLogoutTimer', response.data.auth.expires_in)
        router.push({name: 'home'})     
        return response
      })
      .catch((error) => {
        console.log(error)
        return error
      })
  },
  autologin ({commit}) {
    console.log('0')
    const token = localStorage.getItem('access_token')
    console.log(token)
    const token2 = localStorage.getItem('refresh_token')
    console.log(token2)
    if (!token) { console.log('1')
     return
    }
    const expirationTime2 = new Date(localStorage.getItem('expirationTime'))
    console.log(expirationTime2)
    const now = new Date()
    console.log(now)
    if (now > expirationTime2) {
      console.log('EEEE')
      localStorage.clear()
      
      commit('CLEAR_USER')
      router.push({name: 'login'})
      commit('CLEAR_USERNAME')   
            
      console.log('dfsfd')
    }
    
    if (now <= expirationTime2) {
      console.log('2')
      commit('SET_USER', {user: token})
    commit('REFRESH', {user: token2})
    commit('SET_USERNAME')
    }
    


  },
  logout ({commit}) {
    commit('CLEAR_USERNAME')
    localStorage.removeItem('access_token')
    localStorage.removeItem('refresh_token')
    console.log('state.user.refresh_token')
    console.log(state.user.refresh_token)
    return MaaSaxios.post('/token/logout', {refresh_token: state.user.refresh_token}, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        commit('CLEAR_USER')
        router.push({name: 'login'})
      })
      .catch((error) => {
        console.info(error)
      })
  },
  createDeployment ({commit}, CreatedDeploymentData) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.post('/deployments/', CreatedDeploymentData, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
        return error
      })
  },
  createRegion ({commit}, CreatedDeploymentData) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.post('/regions/', CreatedDeploymentData, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
        return error
      })
  },
  createOperator ({commit}, CreatedDeploymentData) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.post('/operators/', CreatedDeploymentData, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
        return error

      })
  },
  UpdateConfiguration ({commit}, ConfigurationData) {
    // Fetch a list of recipes, paginated
    console.log(ConfigurationData)
    // console.log(ConfigurationData.Id)
    // console.log(ConfigurationData.Object1)
    const id = ConfigurationData[1]
    const ObjectToBeSent = ConfigurationData[0]
    return MaaSaxios.put('/federation/' + id, ObjectToBeSent, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getDeploymentList ({commit}, PageParams) {
    console.log(state.user.access_token)
    console.log(state.user.refresh_token)
    return MaaSaxios.get('/deployments/?page=' + PageParams, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getDeploymentById ({commit}, DeploymentId) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.get('/deployments/' + DeploymentId, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getRegionById ({commit}, RegionId) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.get('/regions/' + RegionId, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getOperators ({commit}) {
    // Fetch a list of operators
    return MaaSaxios.get('/operators/', {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        console.log(response)
        console.log('response')
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getRegions ({commit}) {
    // Fetch a list of operators
    return MaaSaxios.get('/regions/', {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => { 
        console.log('2')
        console.log(response)
        console.log('response')
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getConfiguration ({commit}) {
    // Fetch a list of recipes, paginated
    console.log('22')
    return MaaSaxios.get('/federation/', {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        console.log('23')
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  getOperatorByIdForDeployView ({commit}, OperatorId) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.get('/operators/' + OperatorId, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  },
  deleteDeployInView ({commit}, OperatorId) {
    // Fetch a list of recipes, paginated
    return MaaSaxios.delete('/deployments/' + OperatorId, {headers: {
      'Authorization': 'Bearer ' + state.user.access_token
    }})
      .then(response => {
        return response
      })
      .catch((error) => {
        console.info(error)
      })
  }

}
