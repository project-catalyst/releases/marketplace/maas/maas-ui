// put variables and collections here
export default {
  username: null, // string
  user: {
    access_token: '',
    refresh_token: ''
  } // user: object
}
