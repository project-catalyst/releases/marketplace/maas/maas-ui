import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import DeploymentsView from '../views/DeploymentsView.vue'
import DeploymentView from '../views/SingleDeploymentView.vue'
import ConfigurationView from '../views/ConfigurationView.vue'
import deploymentAdd from '../views/DeploymentAdd.vue'
import PageNotFound from '../views/PageNotFound.vue'
import RegionAdd from '../views/RegionAdd.vue'
import OperatorAdd from '../views/OperatorAdd.vue'
import Operators from '../views/Operators.vue'
import Regions from '../views/Regions.vue'

const originalPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

export default new Router({
  // mode: 'history',
  base: '/catalyst/maas/v1',
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login/',
      name: 'login',
      component: Login
    },
    {
      path: '/workspace/deployments',
      name: 'deployments',
      component: DeploymentsView
    },
    {
      path: '/workspace/deployments/:id',
      name: 'deploymentView',
      component: DeploymentView
    },
    {
      path: '/workspace/configuration',
      name: 'configurationView',
      component: ConfigurationView
    },
    {
      path: '/add',
      name: 'deploymentAdd',
      component: deploymentAdd
    },
    {
      path: '/RegionAdd',
      name: 'RegionAdd',
      component: RegionAdd
    },
    {
      path: '/OperatorAdd',
      name: 'OperatorAdd',
      component: OperatorAdd
    },
    {
      path: '/Operators',
      name: 'Operators',
      component: Operators
    },
    {
      path: '/Regions',
      name: 'Regions',
      component: Regions
    },
    {
      path: '*',
      name: '404',
      component: PageNotFound
    }
  ]
})
