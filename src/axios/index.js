import axios from 'axios'

console.log(process.env.ApiService)
if (process.env.ApiService === '' || process.env.ApiService === undefined || process.env.ApiService === null) {
  var BASEURL = 'http://83.235.169.221:51008/catalyst/maas/api/v1'
} else {
  BASEURL = process.env.ApiService
}

const instance = axios.create({
  baseURL: BASEURL
})
// instance.defaults.headers.common['SOMETHING'] = 'something'
export default instance
